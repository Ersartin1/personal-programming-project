﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject PauseMenuPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P)) {
            if (PauseMenuPanel.active)
            {
                PauseMenuPanel.SetActive(false);
                Time.timeScale = 1;
            } else
            {
                PauseMenuPanel.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }

    public void SaveGame()
    {
        GameSaverAndLoader.SaveGame();
        SceneManager.LoadScene("Title Screen");
    }
}
