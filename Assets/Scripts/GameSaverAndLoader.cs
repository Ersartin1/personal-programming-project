﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSaverAndLoader
{
    public static int gameId;

    public static bool LoadedFromMain = false;
    public static Vector2Int StartingPos;
    public static void SaveGame()
    {
        string path = "Assets/Saved Games/Game " + gameId;
        for (int i = 0; i < SavedDungeon.LastLevel+1; i++) {
            if (SavedDungeon.SavedDungeons[i] != null)
            {
                Stream s = new FileStream(path + "/Level " + i + ".bin", FileMode.OpenOrCreate);
                BinaryWriter bw = new BinaryWriter(s);
                int[,] LevelToSave = SavedDungeon.SavedDungeons[i].SavedDungeonLayout;
                bw.Write(LevelToSave.GetLength(0));
                bw.Write(LevelToSave.GetLength(1));
                for (int j = 0; j < LevelToSave.GetLength(0); j++)
                {
                    for (int k = 0; k < LevelToSave.GetLength(1); k++)
                    {
                        bw.Write((short)LevelToSave[j, k]);
                    }
                }
            }
        }
        {
            Player Player = GameObject.FindObjectOfType<Player>();
            Stream s = new FileStream(path + "/Player.bin", FileMode.OpenOrCreate);
            BinaryWriter bw = new BinaryWriter(s);
            bw.Write(SavedDungeon.currentLevel);
            bw.Write(SavedDungeon.PreviousFloor);
            bw.Write((short)Player.transform.position.x);
            bw.Write((short)Player.transform.position.y);
        }
    }
    public static void LoadGame(int gameId)
    {
        GameSaverAndLoader.gameId = gameId;
        string path = "Assets/Saved Games/Game " + gameId;
        for (int i = 0; i < SavedDungeon.LastLevel + 1; i++)
        {
            if (System.IO.File.Exists(path + "/Level " + i + ".bin"))
            {
                Stream s = new FileStream(path + "/Level " + i + ".bin", FileMode.Open);
                BinaryReader Br = new BinaryReader(s);
                int Length = Br.ReadInt32();
                int Width = Br.ReadInt32();
                SavedDungeon.SavedDungeons[i] = new SavedDungeon();
                SavedDungeon.SavedDungeons[i].SavedDungeonLayout = new int[Length, Width];
                for (int j = 0; j < Length; j++)
                {
                    for (int k = 0; k < Width; k++)
                    {
                        SavedDungeon.SavedDungeons[i].SavedDungeonLayout[j,k] = Br.ReadInt16();
                    }
                }
            }
        }
        {
            Player Player = GameObject.FindObjectOfType<Player>();
            Stream s = new FileStream(path + "/Player.bin", FileMode.Open);
            BinaryReader br = new BinaryReader(s);
            SavedDungeon.currentLevel = br.ReadInt32();
            SavedDungeon.PreviousFloor = br.ReadInt32();
            StartingPos.x = br.ReadInt16();
            StartingPos.y = br.ReadInt16();
        }
        LoadedFromMain = true;
        SceneManager.LoadScene(SavedDungeon.Levels[SavedDungeon.currentLevel]);
    }
}
