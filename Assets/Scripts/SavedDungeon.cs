﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SavedDungeon
{
    // Start is called before the first frame update

    public static string[] Levels = { "Level 0", "Level 1", "Level 2" };

    public static int currentLevel = 0;

    public const int LastLevel = 2;

    public static int PreviousFloor = -1;

    public static SavedDungeon[] SavedDungeons = new SavedDungeon[10];
    
    public int[,] SavedDungeonLayout;

    public int?[,] SavedDungeonObjects;
}
