﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadGameButton : MonoBehaviour
{
    public int gameId;

    // Start is called before the first frame update
    void Start()
    {
        if(!System.IO.File.Exists("Assets/Saved Games/Game " + gameId + "/Level 0.bin"))
        {
            GetComponent<Button>().interactable = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
