﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class DungeonLayoutGenerator : MonoBehaviour
{
    public Tilemap Layout;
    public Tilemap ObjectsLayout;
    public WallTile WallBlueprint;
    public FloorTile FloorBlueprint;
    public DoorTile DoorBlueprint;
    public StairsTile UpStairsBlueprint;
    public StairsTile DownStairsBlueprint;
    public Vector2Int Origin;
    public Vector2Int DungeonSize;
    public List<Vector2Int> EmptyPos = new List<Vector2Int>();
    public List<WallTile> Walls = new List<WallTile>();
    public List<FloorTile> Floors = new List<FloorTile>();
    private List<DoorTile> Doors = new List<DoorTile>();
    public List<Room> Rooms = new List<Room>();
    public int Attempts;
    private List<WallTile> OddWalls = new List<WallTile>();
    public int seed;
    public bool RandomSeed;
    private StairsTile UpStairsTile;
    private StairsTile DownStairsTile;
    public Player Player;
    public Room[] RoomShapes;
    public RoomType[] RoomTypes;
    public static List<CustomTile> Typetable;
    public static List<RoomType> InitailizedRooms = new List<RoomType>();
    public static bool TypesAreInitialized = false;

    // Start is called before the first frame update
    void Start()
    {
        if (!TypesAreInitialized)
        {
            Typetable = new List<CustomTile>() { FloorBlueprint, WallBlueprint, DoorBlueprint, UpStairsBlueprint, DownStairsBlueprint };
            TypesAreInitialized = true;
        }
        foreach (RoomType roomType in RoomTypes)
        {
            if (!InitailizedRooms.Contains(roomType))
            {
                roomType.Initialize();
                InitailizedRooms.Add(roomType);
            }
        }
        if (!RandomSeed)
            Random.InitState(seed);
        if (SavedDungeon.SavedDungeons[SavedDungeon.currentLevel] == null)
            GenerateLayout();
        else
            LoadFromSave();
        if (GameSaverAndLoader.LoadedFromMain)
        {
            Player.PlacePlayer(GameSaverAndLoader.StartingPos);
            GameSaverAndLoader.LoadedFromMain = false;
        }
        else
        {
            if (SavedDungeon.PreviousFloor < SavedDungeon.currentLevel)
                Player.PlacePlayer(UpStairsTile);
            else
                Player.PlacePlayer(DownStairsTile);
        }
    }

    private void LoadFromSave()
    {
        Layout.size = new Vector3Int(DungeonSize.x, DungeonSize.y, 1);
        Layout.origin = new Vector3Int(Origin.x, Origin.y, 0);
        for (int i = 0; i < DungeonSize.x; i++)
        {
            for (int j = 0; j < DungeonSize.y; j++)
            {
                CustomTile Tile = Instantiate((CustomTile)Typetable[SavedDungeon.SavedDungeons[SavedDungeon.currentLevel].SavedDungeonLayout[i, j]]);
                Tile.x = i + Origin.x;
                Tile.y = j + Origin.y;
                Layout.SetTile(new Vector3Int(i + Origin.x, j + Origin.y, 1), Tile);
                StairsTile Stairs = Tile as StairsTile;
                if (Stairs)
                {
                    if(Stairs.StairDirection == StairsTile.Direction.Up)
                    {
                        UpStairsTile = Stairs;
                    } else
                    {
                        DownStairsTile = Stairs;
                    }
                }
                int? index = SavedDungeon.SavedDungeons[SavedDungeon.currentLevel].SavedDungeonObjects[i, j];
                if (index != null)
                {
                    Tile = Instantiate((CustomTile)Typetable[(int)index]);
                    Tile.x = i + Origin.x;
                    Tile.y = j + Origin.y;
                    ObjectsLayout.SetTile(new Vector3Int(i + Origin.x, j + Origin.y, 1), Tile);
                }
            }
        }
    }

    public void GenerateLayout()
    {
        Walls.Clear();
        Floors.Clear();
        Rooms.Clear();
        OddWalls.Clear();
        Layout.ClearAllTiles();
        Layout.size = new Vector3Int(DungeonSize.x, DungeonSize.y, 1);
        Layout.origin = new Vector3Int(Origin.x, Origin.y, 0);
        TileBase[] TileArray = new TileBase[Layout.size.x * Layout.size.y * Layout.size.z];
        for (int index = 0; index < TileArray.Length; index++)
        {
            int x = index % Layout.size.x + Origin.x;
            int y = index / Layout.size.x + Origin.y;
            EmptyPos.Add(new Vector2Int(x, y));
        }
        PlaceRooms();
        foreach (Vector2Int EmptyTile in EmptyPos)
        {
            WallTile Wall = Instantiate(WallBlueprint);
            Wall.x = EmptyTile.x;
            Wall.y = EmptyTile.y;
            Layout.SetTile(new Vector3Int(EmptyTile.x, EmptyTile.y, 1), Wall);
            Walls.Add(Wall);
        }
        EmptyPos.Clear();
        CreateMaze();
        CreateDoors();
        RemoveDeadEnd();
        RemoveSections();
        PopulateRooms();
        GenerateStairs(StairsTile.Direction.Up);
        if(SavedDungeon.currentLevel != SavedDungeon.LastLevel)
            GenerateStairs(StairsTile.Direction.Down);
        SaveDungeon();
    }

    private void PopulateRooms()
    {
        foreach(Room Room in Rooms)
        {
            Room.RoomType = Instantiate(RoomTypes[Random.Range(0, RoomTypes.Length)]);
            Room.RoomType.PopulateRoom(Room, ObjectsLayout, Layout);
        }
    }

    class Section
    {
        public List<CustomTile> WalkableTiles = new List<CustomTile>();
        public List<Room> Rooms = new List<Room>();
    }

    private void RemoveSections()
    {
        List<Section> Sections = new List<Section>();
        List<CustomTile> WalkableTiles = new List<CustomTile>();
        foreach(FloorTile Floor in Floors)
        {
            WalkableTiles.Add(Floor);
        }
        foreach (DoorTile Door in Doors)
        {
            WalkableTiles.Add(Door);
        }
        while (WalkableTiles.Count > 0)
        {
            Section newSection = new Section();
            Queue<CustomTile> WalkableTileQueue = new Queue<CustomTile>();
            WalkableTileQueue.Enqueue(WalkableTiles[0]);
            WalkableTiles.RemoveAt(0);
            while (WalkableTileQueue.Count > 0)
            {
                CustomTile WalkableTile = WalkableTileQueue.Dequeue();
                if (!newSection.Rooms.Contains(WalkableTile.Room))
                {
                    newSection.Rooms.Add(WalkableTile.Room);
                }
                newSection.WalkableTiles.Add(WalkableTile);
                CustomTile AdjacentTileRight = Layout.GetTile(new Vector3Int(WalkableTile.x + 1, WalkableTile.y, 1)) as CustomTile;
                CustomTile AdjacentTileLeft = Layout.GetTile(new Vector3Int(WalkableTile.x - 1, WalkableTile.y, 1)) as CustomTile;
                CustomTile AdjacentTileUp = Layout.GetTile(new Vector3Int(WalkableTile.x, WalkableTile.y + 1, 1)) as CustomTile;
                CustomTile AdjacentTileDown = Layout.GetTile(new Vector3Int(WalkableTile.x, WalkableTile.y - 1, 1)) as CustomTile;
                if (AdjacentTileRight as FloorTile || AdjacentTileRight as DoorTile)
                {
                    if (WalkableTiles.Remove(AdjacentTileRight))
                        WalkableTileQueue.Enqueue(AdjacentTileRight);
                }
                if (AdjacentTileLeft as FloorTile || AdjacentTileLeft as DoorTile)
                {
                    if (WalkableTiles.Remove(AdjacentTileLeft))
                        WalkableTileQueue.Enqueue(AdjacentTileLeft);
                }
                if (AdjacentTileUp as FloorTile || AdjacentTileUp as DoorTile)
                {
                    if (WalkableTiles.Remove(AdjacentTileUp))
                        WalkableTileQueue.Enqueue(AdjacentTileUp);
                }

                if (AdjacentTileDown as FloorTile || AdjacentTileDown as DoorTile)
                {
                    if (WalkableTiles.Remove(AdjacentTileDown))
                        WalkableTileQueue.Enqueue(AdjacentTileDown);
                }
            }
            Sections.Add(newSection);
        }
        Section LargestSection = Sections[0];
        foreach(Section Section in Sections)
        {
            if(Section.WalkableTiles.Count > LargestSection.WalkableTiles.Count)
            {
                LargestSection = Section;
            }
        }
        Sections.Remove(LargestSection);
        foreach (Section Section in Sections)
        {
            foreach (CustomTile Tile in Section.WalkableTiles)
            {
                if(Tile as FloorTile)
                {
                    Floors.Remove(Tile as FloorTile);
                }
                if (Tile as DoorTile)
                {
                    Doors.Remove(Tile as DoorTile);
                }
                WallTile Wall = Instantiate(WallBlueprint);
                Wall.x = Tile.x;
                Wall.x = Tile.y;
                Layout.SetTile(new Vector3Int(Tile.x, Tile.y, 1), Wall);
                Walls.Add(Wall);
                Destroy(Tile);
            }
            foreach(Room Room in Section.Rooms)
            {
                Rooms.Remove(Room);
            }
        }
    }

    private void GenerateStairs(StairsTile.Direction direction)
    {
        Room RandomRoomForStairs = Rooms[Random.Range(0, Rooms.Count)];
        FloorTile FloorToBeReplaced = null;
        while (FloorToBeReplaced == null || !FloorToBeReplaced.isStairable)
        {
            FloorToBeReplaced = RandomRoomForStairs.RoomLayout[Random.Range(0, RandomRoomForStairs.RoomLayout.Length)] as FloorTile;
        }
        StairsTile Stairs;     
        if (direction == StairsTile.Direction.Up)
        {
            Stairs = Instantiate(UpStairsBlueprint);
            UpStairsTile = Stairs;
        } else
        {
            Stairs = Instantiate(DownStairsBlueprint);
            DownStairsTile = Stairs;
        }
        Stairs.x = FloorToBeReplaced.x;
        Stairs.y = FloorToBeReplaced.y;
        Floors.Remove(FloorToBeReplaced);
        RandomRoomForStairs.Floors.Remove(FloorToBeReplaced);
        Layout.SetTile(new Vector3Int(Stairs.x, Stairs.y, 1), Stairs);
        Destroy(FloorToBeReplaced);
    }

    private void SaveDungeon()
    {
        SavedDungeon SavedDungeon = new SavedDungeon();
        SavedDungeon.SavedDungeonLayout = new int[DungeonSize.x, DungeonSize.y];
        SavedDungeon.SavedDungeonObjects = new int?[DungeonSize.x, DungeonSize.y];
        for (int i = 0; i < DungeonSize.x; i++)
        {
            for (int j = 0; j < DungeonSize.y; j++)
            {
                CustomTile Tile = Layout.GetTile(new Vector3Int(i + Origin.x, j + Origin.y, 1)) as CustomTile;
                SavedDungeon.SavedDungeonLayout[i, j] = Tile.tileId;
                Tile = ObjectsLayout.GetTile(new Vector3Int(i + Origin.x, j + Origin.y, 1)) as CustomTile;
                if (Tile == null)
                {
                    SavedDungeon.SavedDungeonObjects[i, j] = null;
                } else
                {
                    SavedDungeon.SavedDungeonObjects[i, j] = Tile.tileId;
                }
            }
        }
        SavedDungeon.SavedDungeons[SavedDungeon.currentLevel] = SavedDungeon;
    }

    private void RemoveDeadEnd()
    {
        Queue<CustomTile> DeadEnds = new Queue<CustomTile>();
        foreach (FloorTile Floor in Floors)
        {
            if (isDeadEnd(Floor))
                DeadEnds.Enqueue(Floor);
        }
        foreach (DoorTile Door in Doors)
        {
            if (isDeadEnd(Door))
                DeadEnds.Enqueue(Door);
        }
        while (DeadEnds.Count > 0)
        {
            CustomTile Floor = DeadEnds.Dequeue();
            WallTile WallToPlace = Instantiate(WallBlueprint);
            WallToPlace.x = Floor.x;
            WallToPlace.y = Floor.y;
            if (Floor as FloorTile)
            {
                Floors.Remove(Floor as FloorTile);
                if(Floor.Room != null)
                    Floor.Room.Floors.Remove(Floor as FloorTile);
            }
            
            if (Floor as DoorTile)
            {
                Doors.Remove(Floor as DoorTile);
                if (Floor.Room != null)
                    Floor.Room.Doors.Remove(Floor as DoorTile);
                FloorTile AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1)) as FloorTile;
                if (AdjacentTile && AdjacentTile.Room) {
                    AdjacentTile.Door = null;
                    AdjacentTile.Room.FloorsNextToDoor.Remove(AdjacentTile);
                }
               AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1)) as FloorTile;
                if (AdjacentTile && AdjacentTile.Room)
                {
                    AdjacentTile.Door = null;
                    AdjacentTile.Room.FloorsNextToDoor.Remove(AdjacentTile);
                }
                AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1)) as FloorTile;
                if (AdjacentTile && AdjacentTile.Room)
                {
                    AdjacentTile.Door = null;
                    AdjacentTile.Room.FloorsNextToDoor.Remove(AdjacentTile);
                }
                AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1)) as FloorTile;
                if (AdjacentTile && AdjacentTile.Room)
                {
                    AdjacentTile.Door = null;
                    AdjacentTile.Room.FloorsNextToDoor.Remove(AdjacentTile);
                }
            }
            {
                Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), WallToPlace);
                CustomTile AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1)) as CustomTile;
                if ((AdjacentTile as FloorTile || AdjacentTile as DoorTile) && isDeadEnd(AdjacentTile))
                    DeadEnds.Enqueue(AdjacentTile);
                AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1)) as CustomTile;
                if ((AdjacentTile as FloorTile || AdjacentTile as DoorTile) && isDeadEnd(AdjacentTile))
                    DeadEnds.Enqueue(AdjacentTile);
                AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1)) as CustomTile;
                if ((AdjacentTile as FloorTile || AdjacentTile as DoorTile) && isDeadEnd(AdjacentTile))
                    DeadEnds.Enqueue(AdjacentTile);
                AdjacentTile = Layout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1)) as CustomTile;
                if ((AdjacentTile as FloorTile || AdjacentTile as DoorTile) && isDeadEnd(AdjacentTile))
                    DeadEnds.Enqueue(AdjacentTile);
                Destroy(Floor);
            }
        }
    }

    private bool isDeadEnd(CustomTile Floor)
    {
        TileBase AdjacentTileRight = Layout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1));
        TileBase AdjacentTileLeft = Layout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1));
        TileBase AdjacentTileUp = Layout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1));
        TileBase AdjacentTileDown = Layout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1));
        int numAdjRooms = 0;

        if (AdjacentTileRight as FloorTile || AdjacentTileRight as DoorTile)
        {
            numAdjRooms++;
        }

        if (AdjacentTileLeft as FloorTile || AdjacentTileLeft as DoorTile)
        {
            numAdjRooms++;
        }

        if (AdjacentTileUp as FloorTile || AdjacentTileUp as DoorTile)
        {
            numAdjRooms++;
        }

        if (AdjacentTileDown as FloorTile || AdjacentTileDown as DoorTile)
        {
            numAdjRooms++;
        }

        if(numAdjRooms > 1)
        {
            return false;
        }
        return true;
    }

    private void CreateDoors()
    {
        foreach (Room Room in Rooms)
        {
            int count = 0;
            foreach (TileBase Tile in Room.RoomLayout)
            {
                if (Tile != null)
                {
                    CustomTile CustomTile = Tile as CustomTile;
                    WallTile Border = Layout.GetTile(new Vector3Int(CustomTile.x, CustomTile.y, 1)) as WallTile;
                    if (Border && Border.isDoorable)
                    {
                        Room.DoorableWalls.Add(new KeyValuePair<WallTile, int>(Border, count));
                    }
                    count++;
                }
            }
            int numDoors = Random.Range(1,3);
            for (int i = 0; i < numDoors; i++)
            {
                int randomindex = Random.Range(0, Room.DoorableWalls.Count);
                KeyValuePair<WallTile, int> Pair = Room.DoorableWalls[randomindex];
                Room.DoorableWalls.RemoveAt(randomindex);
                int roomindex = Pair.Value;
                WallTile WallToDoor = Pair.Key;
                DoorTile DoorToPlace = Instantiate(DoorBlueprint);
                DoorToPlace.x = WallToDoor.x;
                DoorToPlace.y = WallToDoor.y;
                Room.RoomLayout[roomindex] = DoorToPlace;
                DoorToPlace.Room = Room;
                Walls.Remove(WallToDoor);
                Room.Walls.Remove(WallToDoor);
                Doors.Add(DoorToPlace);
                Room.Doors.Add(DoorToPlace);
                Layout.SetTile(new Vector3Int(WallToDoor.x, WallToDoor.y, 1), DoorToPlace);
                Destroy(WallToDoor);
                FloorTile AdjacentTileRight = Layout.GetTile(new Vector3Int(DoorToPlace.x + 1, DoorToPlace.y, 1)) as FloorTile;
                FloorTile AdjacentTileLeft = Layout.GetTile(new Vector3Int(DoorToPlace.x - 1, DoorToPlace.y, 1)) as FloorTile;
                FloorTile AdjacentTileUp = Layout.GetTile(new Vector3Int(DoorToPlace.x, DoorToPlace.y + 1, 1)) as FloorTile;
                FloorTile AdjacentTileDown = Layout.GetTile(new Vector3Int(DoorToPlace.x, DoorToPlace.y - 1, 1)) as FloorTile;
                if (AdjacentTileRight && AdjacentTileRight.Room)
                {
                    AdjacentTileRight.Door = DoorToPlace;
                    AdjacentTileRight.DoorDirection = FloorTile.Direction.Right;
                    AdjacentTileRight.Room.FloorsNextToDoor.Add(AdjacentTileRight);
                }
                if (AdjacentTileLeft && AdjacentTileLeft.Room)
                {
                    AdjacentTileLeft.Door = DoorToPlace;
                    AdjacentTileLeft.DoorDirection = FloorTile.Direction.Left;
                    AdjacentTileLeft.Room.FloorsNextToDoor.Add(AdjacentTileLeft);
                }
                if (AdjacentTileUp && AdjacentTileUp.Room)
                {
                    AdjacentTileUp.Door = DoorToPlace;
                    AdjacentTileUp.DoorDirection = FloorTile.Direction.Up;
                    AdjacentTileUp.Room.FloorsNextToDoor.Add(AdjacentTileUp);
                }
                if (AdjacentTileDown && AdjacentTileDown.Room)
                {
                    AdjacentTileDown.Door = DoorToPlace;
                    AdjacentTileDown.DoorDirection = FloorTile.Direction.Down;
                    AdjacentTileDown.Room.FloorsNextToDoor.Add(AdjacentTileDown);
                }
            }
        }
    }

    void PlaceRooms()
    {
        for(int i = 0; i < Attempts; i++)
        {
            PlaceRoom();
        }
    }

    void PlaceRoom()
    {
        int x = Random.Range((Layout.cellBounds.xMin)/2, (Layout.cellBounds.xMax)/2)*2;
        int y = Random.Range((Layout.cellBounds.yMin)/2, (Layout.cellBounds.yMax)/2)*2;
        Room Room = RoomShapes[Random.Range(0, RoomShapes.Length)].CreateRoom();
        Room.Position = new Vector2Int(x, y);
        if(!Room.CheckRoomFitsInDungeon(x, y, Layout))
        {
            DestroyImmediate(Room);
            return;
        }
        Room.PlaceRoomInDungeon(x, y, this);
    }

    public bool isDungeonEdge(int x, int y)
    {
        if (x <= Layout.cellBounds.xMin || y <= Layout.cellBounds.yMin || x >= Layout.cellBounds.xMax-1 || y >= Layout.cellBounds.yMax-1)
        {
            return true;
        }
        return false;
    }

    void CreateMaze()
    {
        foreach (WallTile WallTile in Walls)
        {
            if ((WallTile.x % 2 == 1 || WallTile.x % 2 == -1) && (WallTile.y % 2 == 1 || WallTile.y % 2 == -1) && WallTile.isDestructible)
            {
                OddWalls.Add(WallTile);
            }
        }
        while (OddWalls.Count > 0)
        {
            WallTile StartingWall = OddWalls[Random.Range(0, OddWalls.Count)];
            CreateMaze(StartingWall);
        }
    }

    enum Direction { Right, Left, Up, Down };

    class WallandPrevious{
        public WallandPrevious(WallTile CurrentWall, WallTile PreviousWall, WallTile InBetweenWall)
        {
            this.CurrentWall = CurrentWall;
            this.PreviousWall = PreviousWall;
            this.InBetweenWall = InBetweenWall;
        }
        public WallTile CurrentWall;
        public WallTile PreviousWall;
        public WallTile InBetweenWall;
    }
    void CreateMaze(WallTile StartingWall)
    {
        Stack<WallandPrevious> MazeStack = new Stack<WallandPrevious>();
        WallandPrevious Start = new WallandPrevious(StartingWall, null, null);
        MazeStack.Push(Start);
        FloorTile Floor;
        while (MazeStack.Count > 0) {
            WallandPrevious Current = MazeStack.Pop();
            WallTile CurrentWall = Current.CurrentWall;
            bool ShouldBreak = false;

            while (!OddWalls.Contains(CurrentWall)) {
                if (MazeStack.Count == 0)
                {
                    ShouldBreak = true;
                    break;
                }
                Current = MazeStack.Pop();
                CurrentWall = Current.CurrentWall;
            }
            if (ShouldBreak)
            {
                break;
            }

            Walls.Remove(CurrentWall);
            OddWalls.Remove(CurrentWall);
            Floor = Instantiate(FloorBlueprint);
            Floor.x = CurrentWall.x;
            Floor.y = CurrentWall.y;
            Layout.SetTile(new Vector3Int(CurrentWall.x, CurrentWall.y, 1), Floor);
            Floors.Add(Floor);

            WallTile PreviousWall = Current.PreviousWall;

            if (PreviousWall)
            {
                WallTile InBetweenWall = Current.InBetweenWall;
                Walls.Remove(InBetweenWall);
                Floor = Instantiate(FloorBlueprint);
                Floor.x = InBetweenWall.x;
                Floor.y = InBetweenWall.y;
                Layout.SetTile(new Vector3Int(InBetweenWall.x, InBetweenWall.y, 1), Floor);
                Floors.Add(Floor);
                Destroy(InBetweenWall);
            }

            List<Direction> Directions = new List<Direction>(new Direction[] { Direction.Right, Direction.Left, Direction.Up, Direction.Down });
            for (int i = 0; i < 4; i++)
            {
                int randomIndex = Random.Range(0, Directions.Count);
                Direction randomDirection = Directions[randomIndex];
                Directions.RemoveAt(randomIndex);
                WallTile NextWall;
                WallTile InBetweenWall;
                switch (randomDirection)
                {
                    case Direction.Right:
                        NextWall = Layout.GetTile(new Vector3Int(CurrentWall.x + 2, CurrentWall.y, 1)) as WallTile;
                        if (OddWalls.Contains(NextWall) && NextWall.isDestructible)
                        {
                            InBetweenWall = Layout.GetTile(new Vector3Int(CurrentWall.x + 1, CurrentWall.y, 1)) as WallTile;
                            if (InBetweenWall)
                            {
                                WallandPrevious Next = new WallandPrevious(NextWall, CurrentWall, InBetweenWall);
                                MazeStack.Push(Next);
                            }
                        }
                        break;
                    case Direction.Left:
                        NextWall = Layout.GetTile(new Vector3Int(CurrentWall.x - 2, CurrentWall.y, 1)) as WallTile;
                        if (OddWalls.Contains(NextWall) && NextWall.isDestructible)
                        {
                            InBetweenWall = Layout.GetTile(new Vector3Int(CurrentWall.x - 1, CurrentWall.y, 1)) as WallTile;
                            if (InBetweenWall)
                            {
                                WallandPrevious Next = new WallandPrevious(NextWall, CurrentWall, InBetweenWall);
                                MazeStack.Push(Next);
                            }
                        }
                        break;
                    case Direction.Down:
                        NextWall = Layout.GetTile(new Vector3Int(CurrentWall.x, CurrentWall.y - 2, 1)) as WallTile;
                        if (OddWalls.Contains(NextWall) && NextWall.isDestructible)
                        {
                            InBetweenWall = Layout.GetTile(new Vector3Int(CurrentWall.x, CurrentWall.y - 1, 1)) as WallTile;
                            if (InBetweenWall)
                            {
                                WallandPrevious Next = new WallandPrevious(NextWall, CurrentWall, InBetweenWall);
                                MazeStack.Push(Next);
                            }
                        }
                        break;
                    case Direction.Up:
                        NextWall = Layout.GetTile(new Vector3Int(CurrentWall.x, CurrentWall.y + 2, 1)) as WallTile;
                        if (OddWalls.Contains(NextWall) && NextWall.isDestructible)
                        {
                            InBetweenWall = Layout.GetTile(new Vector3Int(CurrentWall.x, CurrentWall.y + 1, 1)) as WallTile;
                            if (InBetweenWall)
                            {
                                WallandPrevious Next = new WallandPrevious(NextWall, CurrentWall, InBetweenWall);
                                MazeStack.Push(Next);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
