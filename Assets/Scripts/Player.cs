﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Tilemap LayoutTilemap;
    public Tilemap CharacterTilemap;
    public Tilemap ObjectTilemap;
    public PlayerTile PlayerTileBlueprint;
    PlayerTile PlayerTileInstance;
    private static bool CanPhase = false;
    [SerializeField]
    private bool canPhase;

    enum Direction { Right, Left, Up, Down, UpRight, UpLeft, DownRight, DownLeft};

    public void PlacePlayer(StairsTile StartingStairs)
    {
        PlayerTileInstance = Instantiate(PlayerTileBlueprint);
        PlayerTileInstance.x = StartingStairs.x;
        PlayerTileInstance.y = StartingStairs.y;
        transform.position = new Vector3Int(StartingStairs.x, StartingStairs.y, 0);
        CharacterTilemap.SetTile(new Vector3Int(StartingStairs.x, StartingStairs.y, 1), PlayerTileInstance);
    }

    public void PlacePlayer(Vector2Int Position)
    {
        PlayerTileInstance = Instantiate(PlayerTileBlueprint);
        PlayerTileInstance.x = Position.x;
        PlayerTileInstance.y = Position.y;
        transform.position = new Vector3Int(Position.x, Position.y, 0);
        CharacterTilemap.SetTile(new Vector3Int(Position.x, Position.y, 1), PlayerTileInstance);
    }

    void Move(Direction Direction)
    {
        CharacterTilemap.SetTile(new Vector3Int(PlayerTileInstance.x, PlayerTileInstance.y, 1), null);
        int newx = PlayerTileInstance.x;
        int newy = PlayerTileInstance.y;
        switch (Direction)
        {
            case Direction.Right:
                newx += 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.x = newx;
                }
                break;
            case Direction.Left:
                newx -= 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.x = newx;
                }
                break;
            case Direction.Down:
                newy -= 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.y = newy;
                }
                break;
            case Direction.Up:
                newy += 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.y = newy;
                }
                break;
            case Direction.UpRight:
                newx += 1;
                newy += 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.x = newx;
                    PlayerTileInstance.y = newy;
                }
                break;
            case Direction.UpLeft:
                newx -= 1;
                newy += 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.x = newx;
                    PlayerTileInstance.y = newy;
                }
                break;
            case Direction.DownLeft:
                newx -= 1;
                newy -= 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.x = newx;
                    PlayerTileInstance.y = newy;
                }
                break;
            case Direction.DownRight:
                newx += 1;
                newy -= 1;
                if (!Collides(newx, newy))
                {
                    PlayerTileInstance.x = newx;
                    PlayerTileInstance.y = newy;
                }
                break;
            default:
                break;
        }
        transform.position = new Vector3Int(PlayerTileInstance.x, PlayerTileInstance.y, 0);
        CharacterTilemap.SetTile(new Vector3Int(PlayerTileInstance.x, PlayerTileInstance.y, 1), PlayerTileInstance);
    }

    private bool Collides(int x, int y)
    {
        CustomTile Object = ObjectTilemap.GetTile(new Vector3Int(x, y, 1)) as CustomTile;
        return (((LayoutTilemap.GetTile(new Vector3Int(x, y, 1)) as CustomTile).BlocksCharacters || (Object && Object.BlocksCharacters)) && !canPhase) || isDungeonEdge(x,y);
    }
    private bool isDungeonEdge(int x, int y)
    {
        if (x <= LayoutTilemap.cellBounds.xMin || y <= LayoutTilemap.cellBounds.yMin || x >= LayoutTilemap.cellBounds.xMax - 1 || y >= LayoutTilemap.cellBounds.yMax - 1)
        {
            return true;
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        canPhase = CanPhase;
    }

    // Update is called once per frame
    void Update()
    {
        if(CanPhase != canPhase)
            CanPhase = canPhase;
        if (CheckForMoveIsDone)
        {
            StartCoroutine(CheckForMove());
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StairsTile Stairs = LayoutTilemap.GetTile(new Vector3Int(PlayerTileInstance.x, PlayerTileInstance.y, 1)) as StairsTile;
            if (Stairs)
            {
                SavedDungeon.PreviousFloor = SavedDungeon.currentLevel;
                if (Stairs.StairDirection == StairsTile.Direction.Up)
                {
                    if (SavedDungeon.currentLevel != 0)
                    {
                        SceneManager.LoadScene(SavedDungeon.Levels[SavedDungeon.currentLevel - 1]);
                        SavedDungeon.currentLevel--;
                    }
                } else
                {
                    SceneManager.LoadScene(SavedDungeon.Levels[SavedDungeon.currentLevel + 1]);
                    SavedDungeon.currentLevel++;
                }
            }
        }
    }

    bool CheckForMoveIsDone = true;
    IEnumerator CheckForMove()
    {
        CheckForMoveIsDone = false;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (Input.GetKey(KeyCode.RightArrow))
                Move(Direction.UpRight);
            else if (Input.GetKey(KeyCode.LeftArrow))
                Move(Direction.UpLeft);
            else
                Move(Direction.Up);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            if (Input.GetKey(KeyCode.RightArrow))
                Move(Direction.DownRight);
            else if (Input.GetKey(KeyCode.LeftArrow))
                Move(Direction.DownLeft);
            else
                Move(Direction.Down);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            Move(Direction.Right);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            Move(Direction.Left);
        }
        yield return new WaitForSeconds(0.2f);
        CheckForMoveIsDone = true;
    }
}
