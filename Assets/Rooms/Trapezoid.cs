﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Trapezoid : Room
{
    public bool Horizontal;
    public override Room CreateRoom()
    {
        Trapezoid newRoom = Instantiate<Room>(this) as Trapezoid;
        newRoom.Horizontal = (Random.value > 0.5);
        if (newRoom.Horizontal)
        {
            newRoom.RoomHeight = Random.Range(5, 9) / 2 * 2 + 1;
            newRoom.RoomWidth = Random.Range(newRoom.RoomHeight * 2 - 1, MaxRoomSize.x) / 2 * 2 + 1;
        }
        else
        {
            newRoom.RoomWidth = Random.Range(5, 9) / 2 * 2 + 1;
            newRoom.RoomHeight = Random.Range(newRoom.RoomWidth * 2 - 1, MaxRoomSize.y) / 2 * 2 + 1;
        }
        return newRoom;
    }
    public override void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        RoomLayout = new TileBase[RoomWidth * RoomHeight * 1];
        bool Mirror = (Random.value > 0.5);
        for (int i = 0; i < RoomWidth; i++)
        {
            int startingj = 0;
            int endingj = RoomHeight;
            if (Horizontal)
            {
                if (!Mirror)
                {
                    if (i < RoomHeight)
                    {
                        endingj = i + 1;
                    }
                    else if (i > RoomWidth - RoomHeight)
                    {
                        endingj = RoomWidth - i;
                    }
                }
                else
                {
                    if (i < RoomHeight)
                    {
                        startingj = RoomHeight - i;
                    }
                    else if (i > RoomWidth - RoomHeight)
                    {
                        startingj = i - (RoomWidth - RoomHeight);
                    }
                }
            }
            else
            {
                if (!Mirror)
                {
                    startingj = i;
                    endingj = RoomHeight - i;
                }
                else
                {
                    startingj = RoomWidth - i;
                    endingj = RoomHeight - RoomWidth + i;
                }
            }
            for (int j = startingj; j < endingj; j++)
            {
                Vector2Int pos = new Vector2Int(i + x, j + y);
                DLG.EmptyPos.Remove(pos);
                ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                if (isBorder(i, j, 0, RoomWidth - 1, startingj, endingj - 1))
                {
                    if (!(tile as WallTile))
                    {
                        WallTile Wall = Instantiate(DLG.WallBlueprint);
                        Wall.x = i + x;
                        Wall.y = j + y;
                        Wall.isDestructible = false;
                        tile = Wall;
                        DLG.Walls.Add(Wall);
                        Wall.Room = this;
                        Walls.Add(Wall);
                    }
                    WallTile CurrentWall = tile as WallTile;
                    if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                    {
                        CurrentWall.isDoorable = true;
                    }
                }
                else
                {
                    FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                    Floor.x = i + x;
                    Floor.y = j + y;
                    tile = Floor;
                    DLG.Floors.Add(Floor);
                    Floor.Room = this;
                    Floors.Add(Floor);
                }
                DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
            }
        }
        DLG.Rooms.Add(this);
    }
}