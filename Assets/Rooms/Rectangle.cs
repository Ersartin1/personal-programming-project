﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Rectangle : Room
{
    public override void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        RoomLayout = new TileBase[RoomWidth * RoomHeight * 1];
        for (int i = 0; i < RoomWidth; i++)
        {
            for (int j = 0; j < RoomHeight; j++)
            {
                Vector2Int pos = new Vector2Int(i + x, j + y);
                DLG.EmptyPos.Remove(pos);
                if (isBorder(i, j, 0, RoomWidth - 1, 0, RoomHeight - 1))
                {
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    if (!(tile as WallTile))
                    {
                        WallTile Wall = Instantiate(DLG.WallBlueprint);
                        Wall.x = i + x;
                        Wall.y = j + y;
                        tile = Wall;
                        DLG.Walls.Add(Wall);
                        Wall.Room = this;
                        Walls.Add(Wall);
                    }
                    WallTile CurrentWall = tile as WallTile;
                    if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                    {
                        CurrentWall.isDoorable = true;
                    }
                }
                else
                {
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                    Floor.x = i + x;
                    Floor.y = j + y;
                    tile = Floor;
                    DLG.Floors.Add(Floor);
                    Floor.Room = this;
                    Floors.Add(Floor);
                }
            }
        }
        base.PlaceRoomInDungeon(x, y, DLG);
    }
}
