﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class UShape : Room
{
    public override void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        RoomLayout = new TileBase[RoomWidth * RoomHeight * 1];
        bool Top = (Random.value > 0.5);
        int startingj = 0;
        int endingj = RoomHeight;
        if (Top)
        {
            startingj = Random.Range(3, RoomHeight - 2);
        } else
        {
            endingj = Random.Range(3, RoomHeight - 2);
        }
        int BlockStartx = Random.Range(3, RoomWidth - 4)/2*2;
        int BlockEndx = Random.Range(BlockStartx + 2, RoomWidth - 2)/2*2;
        for (int i = 0; i < RoomWidth; i++)
        {
            if (i <= BlockStartx || i >= BlockEndx)
            {
                for (int j = 0; j < RoomHeight; j++)
                {
                    Vector2Int pos = new Vector2Int(i + x, j + y);
                    DLG.EmptyPos.Remove(pos);
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    if (isBorder(i, j, 0, RoomWidth - 1, 0, RoomHeight - 1) || ((i == BlockStartx || i == BlockEndx) && ((Top && j <= startingj) || (!Top && j >= endingj - 1))))
                    {
                        if (!(tile as WallTile))
                        {
                            WallTile Wall = Instantiate(DLG.WallBlueprint);
                            Wall.x = i + x;
                            Wall.y = j + y;
                            tile = Wall;
                            DLG.Walls.Add(Wall);
                            Wall.Room = this;
                            Walls.Add(Wall);
                        }
                        WallTile CurrentWall = tile as WallTile;
                        if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                        {
                            CurrentWall.isDoorable = true;
                        }
                    }
                    else
                    {
                        FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                        Floor.x = i + x;
                        Floor.y = j + y;
                        tile = Floor;
                        DLG.Floors.Add(Floor);
                        Floor.Room = this;
                        Floors.Add(Floor);
                    }
                    DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
                }
            } else
            {
                for (int j = startingj; j < endingj; j++)
                {
                    Vector2Int pos = new Vector2Int(i + x, j + y);
                    DLG.EmptyPos.Remove(pos);
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    if (isBorder(i, j, 0, RoomWidth - 1, startingj, endingj - 1))
                    {
                        if (!(tile as WallTile))
                        {
                            WallTile Wall = Instantiate(DLG.WallBlueprint);
                            Wall.x = i + x;
                            Wall.y = j + y;
                            tile = Wall;
                            DLG.Walls.Add(Wall);
                            Wall.Room = this;
                            Walls.Add(Wall);
                        }
                        WallTile CurrentWall = tile as WallTile;
                        if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                        {
                            CurrentWall.isDoorable = true;
                        }
                    }
                    else
                    {
                        FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                        Floor.x = i + x;
                        Floor.y = j + y;
                        tile = Floor;
                        DLG.Floors.Add(Floor);
                        Floor.Room = this;
                        Floors.Add(Floor);
                    }
                    DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
                }
            }
        }
        DLG.Rooms.Add(this);
    }
}