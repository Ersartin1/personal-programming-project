﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SShape : Room
{
    public override void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        RoomLayout = new TileBase[RoomWidth * RoomHeight * 1];
        bool Left = (Random.value > 0.5);
        int Block1Starty = Random.Range(0, RoomHeight - 5) / 2 * 2;
        int Block1Endy = Random.Range(Block1Starty + 5, RoomHeight) / 2 * 2 + 1;
        int Block2Starty = Random.Range(0, RoomHeight - 5) / 2 * 2;
        int Block2Endy = Random.Range(Block1Starty + 5, RoomHeight) / 2 * 2 + 1;
        int BlockStartx = Random.Range(2, RoomWidth - 8) / 2 * 2;
        int BlockEndx = Random.Range(BlockStartx + 6, RoomWidth - 2) / 2 * 2 + 1;
        PlaceBlock(new Vector2Int(BlockStartx, 0), new Vector2Int(BlockEndx, RoomHeight), x, y, DLG);
        PlaceBlock(new Vector2Int(0, Block1Starty), new Vector2Int(BlockEndx, Block1Endy), x, y, DLG);
        PlaceBlock(new Vector2Int(BlockStartx, Block2Starty), new Vector2Int(RoomWidth, Block2Endy), x, y, DLG);
        DLG.Rooms.Add(this);
    }

    void PlaceBlock(Vector2Int Start, Vector2Int End, int x, int y, DungeonLayoutGenerator DLG)
    {
        for (int i = Start.x; i < End.x; i++)
        {
            for (int j = Start.y; j < End.y; j++)
            {
                Vector2Int pos = new Vector2Int(i + x, j + y);
                if (DLG.EmptyPos.Contains(pos) || RoomLayout[RoomWidth * j + i] as WallTile)
                {
                    DLG.EmptyPos.Remove(pos);
                    Walls.Remove(RoomLayout[RoomWidth * j + i] as WallTile);
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    if (isBorder(i, j, Start.x, End.x - 1, Start.y, End.y - 1))
                    {
                        if (!(tile as WallTile))
                        {
                            WallTile Wall = Instantiate(DLG.WallBlueprint);
                            Wall.x = i + x;
                            Wall.y = j + y;
                            tile = Wall;
                            DLG.Walls.Add(Wall);
                            Wall.Room = this;
                            Walls.Add(Wall);
                        }
                        WallTile CurrentWall = tile as WallTile;
                        if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                        {
                            CurrentWall.isDoorable = true;
                        }
                    }
                    else
                    {
                        FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                        Floor.x = i + x;
                        Floor.y = j + y;
                        tile = Floor;
                        DLG.Floors.Add(Floor);
                        Floor.Room = this;
                        Floors.Add(Floor);
                    }
                    DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
                }
            }
        }
    }
}