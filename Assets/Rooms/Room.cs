﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Room : MonoBehaviour
{
    // Start is called before the first frame update

    public TileBase[] RoomLayout;
    public List<WallTile> Walls = new List<WallTile>();
    public List<FloorTile> Floors = new List<FloorTile>();
    public List<DoorTile> Doors = new List<DoorTile>();
    public List<KeyValuePair<WallTile, int>> DoorableWalls = new List<KeyValuePair<WallTile, int>>();
    public Vector2Int MinRoomSize;
    public Vector2Int MaxRoomSize;
    public int RoomWidth;
    public int RoomHeight;
    public RoomType RoomType;
    public List<FloorTile> FloorsNextToDoor = new List<FloorTile>();
    public Vector2Int Position;
    public virtual Room CreateRoom()
    {
        Room newRoom = Instantiate<Room>(this);
        newRoom.RoomWidth = Random.Range(MinRoomSize.x / 2, (MaxRoomSize.x) / 2) * 2 + 1;
        newRoom.RoomHeight = Random.Range(MinRoomSize.y / 2, (MaxRoomSize.y) / 2) * 2 + 1;
        return newRoom;
    }
    public bool CheckRoomFitsInDungeon(int x, int y, Tilemap Layout)
    {
        if (x + RoomWidth > Layout.cellBounds.xMax)
        {
            return false;
        }
        if (y + RoomHeight > Layout.cellBounds.yMax)
        {
            return false;
        }
        for (int i = x; i < x + RoomWidth; i++)
        {
            for (int j = y; j < y + RoomHeight; j++)
            {
                if (Layout.GetTile(new Vector3Int(i, j, 1)) as FloorTile)
                {
                    return false;
                }
                else if (!isBorder(i, j, x, x + RoomWidth - 1, y, y + RoomHeight - 1) && Layout.GetTile(new Vector3Int(i, j, 1)) as WallTile)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public virtual void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        DLG.Rooms.Add(this);
        BoundsInt RoomBounds = new BoundsInt(x, y, 1, RoomWidth, RoomHeight, 1);
        DLG.Layout.SetTilesBlock(RoomBounds, RoomLayout);
    }

    protected bool isBorder(int x, int y, int x1, int x2, int y1, int y2)
    {
        if (x <= x1 || x >= x2 || y <= y1 || y >= y2)
        {
            return true;
        }
        return false;
    }
}

    
