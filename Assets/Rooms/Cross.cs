﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Cross : Room
{
    public override void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        RoomLayout = new TileBase[RoomWidth * RoomHeight * 1];
        
        int VerticalRoomXStart = Random.Range(0, RoomWidth - 5)/2*2;
        int VerticalRoomXEnd = Random.Range(VerticalRoomXStart + 5, VerticalRoomXStart + 7) / 2 * 2 + 1;
        if(VerticalRoomXEnd > RoomWidth)
        {
            VerticalRoomXEnd = RoomWidth;
        }
        for (int i = VerticalRoomXStart; i < VerticalRoomXEnd; i++)
        {
            for (int j = 0; j < RoomHeight; j++)
            {
                Vector2Int pos = new Vector2Int(i + x, j + y);
                DLG.EmptyPos.Remove(pos);
                ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                if (isBorder(i, j, VerticalRoomXStart, VerticalRoomXEnd-1, 0, RoomHeight - 1))
                {
                    if (!(tile as WallTile))
                    {
                        WallTile Wall = Instantiate(DLG.WallBlueprint);
                        Wall.x = i + x;
                        Wall.y = j + y;
                        tile = Wall;
                        DLG.Walls.Add(Wall);
                        Wall.Room = this;
                        Walls.Add(Wall);
                    }
                    WallTile CurrentWall = tile as WallTile;
                    if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                    {
                        CurrentWall.isDoorable = true;
                    }
                }
                else
                {
                    FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                    Floor.x = i + x;
                    Floor.y = j + y;
                    tile = Floor;
                    DLG.Floors.Add(Floor);
                    Floor.Room = this;
                    Floors.Add(Floor);
                }
                DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
            }
        }

        int HorizontalRoomYStart = Random.Range(0, RoomHeight - 5)/2*2;
        int HorizontalRoomYEnd = Random.Range(HorizontalRoomYStart + 5, HorizontalRoomYStart + 7) / 2 * 2 + 1;
        if (HorizontalRoomYEnd > RoomHeight)
        {
            HorizontalRoomYEnd = RoomHeight;
        }
        for (int i = 0; i < RoomWidth; i++)
        {
            for (int j = HorizontalRoomYStart; j < HorizontalRoomYEnd; j++)
            {
                Vector2Int pos = new Vector2Int(i + x, j + y);
                if (DLG.EmptyPos.Contains(pos) || RoomLayout[RoomWidth * j + i] as WallTile)
                {
                    DLG.EmptyPos.Remove(pos);
                    Walls.Remove(RoomLayout[RoomWidth * j + i] as WallTile);
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    if (isBorder(i, j, 0, RoomWidth - 1, HorizontalRoomYStart, HorizontalRoomYEnd-1))
                    {
                        if (!(tile as WallTile))
                        {
                            WallTile Wall = Instantiate(DLG.WallBlueprint);
                            Wall.x = i + x;
                            Wall.y = j + y;
                            tile = Wall;
                            DLG.Walls.Add(Wall);
                            Wall.Room = this;
                            Walls.Add(Wall);
                        }
                        WallTile CurrentWall = tile as WallTile;
                        if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                        {
                            CurrentWall.isDoorable = true;
                        }
                    }
                    else
                    {
                        FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                        Floor.x = i + x;
                        Floor.y = j + y;
                        tile = Floor;
                        DLG.Floors.Add(Floor);
                        Floor.Room = this;
                        Floors.Add(Floor);
                    }
                    DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y,1), tile);
                }
            }
        }
        DLG.Rooms.Add(this);
    }
}
