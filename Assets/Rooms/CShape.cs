﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CShape : Room
{
    public override void PlaceRoomInDungeon(int x, int y, DungeonLayoutGenerator DLG)
    {
        RoomLayout = new TileBase[RoomWidth * RoomHeight * 1];
        bool Left = (Random.value > 0.5);
        int startingj = Random.Range(3, RoomHeight - 4)/2*2;
        int endingj = Random.Range(startingj + 2, RoomHeight - 2)/2*2;
        int BlockStartx = 0;
        int BlockEndx = RoomWidth;
        if (Left)
        {
            BlockStartx = Random.Range(3, RoomWidth - 2) / 2 * 2+1;
        }
        else
        {
            BlockEndx = Random.Range(3, RoomWidth - 2) / 2 * 2;
        }
        for (int i = 0; i < RoomWidth; i++)
        {
            for (int j = 0; j < RoomHeight; j++)
            {
                if (!(i >= BlockStartx && i < BlockEndx && j > startingj && j < endingj))
                {
                    Vector2Int pos = new Vector2Int(i + x, j + y);
                    DLG.EmptyPos.Remove(pos);
                    ref TileBase tile = ref RoomLayout[RoomWidth * j + i];
                    if (isBorder(i, j, 0, RoomWidth - 1, 0, RoomHeight - 1) || ((j >= startingj && j  <= endingj) && ((Left && i >= BlockStartx-1) || (!Left && i <= BlockEndx))))
                    {
                        if (!(tile as WallTile))
                        {
                            WallTile Wall = Instantiate(DLG.WallBlueprint);
                            Wall.x = i + x;
                            Wall.y = j + y;
                            tile = Wall;
                            DLG.Walls.Add(Wall);
                            Wall.Room = this;
                            Walls.Add(Wall);
                        }
                        WallTile CurrentWall = tile as WallTile;
                        if (!(DLG.isDungeonEdge(i + x, j + y) || (i % 2 == 0 && j % 2 == 0)))
                        {
                            CurrentWall.isDoorable = true;
                        }
                    }
                    else
                    {
                        FloorTile Floor = Instantiate(DLG.FloorBlueprint);
                        Floor.x = i + x;
                        Floor.y = j + y;
                        tile = Floor;
                        DLG.Floors.Add(Floor);
                        Floor.Room = this;
                        Floors.Add(Floor);
                    }
                    DLG.Layout.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
                }
            }
        }
        DLG.Rooms.Add(this);
    }
}