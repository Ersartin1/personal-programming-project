﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

public class FloorTile : CustomTile
{
    public enum Direction { Right, Left, Up, Down };
    
    public bool isStairable = true;
    public Direction? DoorDirection;
    public DoorTile Door;

    [MenuItem("Assets/Create/Custom Tiles/Floor Tile")]
    public static void CreatFloorTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Floor Tile", "New Floor Tile", "asset", "Save Floor Tile", "Assets/Tiles");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<FloorTile>(), path);
    }

}