﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

public class CustomTile : Tile
{
    public static int numberOfTileTypes = 5;
    public int tileId;
    public int x;
    public int y;
    public bool BlocksCharacters;
    public Room Room = null;

    [MenuItem("Assets/Create/Custom Tiles/Custom Tile")]
    public static void CreatCustomTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Custom Tile", "New Custom Tile", "asset", "Save Custom Tile");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<CustomTile>(), path);
    }
}
