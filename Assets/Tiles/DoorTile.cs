﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

public class DoorTile : CustomTile
{
    public FloorTile FloorNextToDoor;

    [MenuItem("Assets/Create/Custom Tiles/Door Tile")]
    public static void CreatDoorTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Door Tile", "New Door Tile", "asset", "Save Door Tile", "Assets/Tiles");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<DoorTile>(), path);
    }

}