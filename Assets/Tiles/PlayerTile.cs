﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

public class PlayerTile : CustomTile
{
    public bool isOpen;

    [MenuItem("Assets/Create/Custom Tiles/Player Tile")]
    public static void CreatPlayerTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Player Tile", "New Player Tile", "asset", "Save Player Tile", "Assets/Tiles");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<PlayerTile>(), path);
    }

}