﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

public class WallTile : CustomTile
{
    public bool isDoorable;
    public bool isDestructible = true;

    [MenuItem("Assets/Create/Custom Tiles/Wall Tile")]
    public static void CreatWallTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Wall Tile", "New Wall Tile", "asset", "Save Wall Tile", "Assets/Tiles");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<WallTile>(), path);
    }

}