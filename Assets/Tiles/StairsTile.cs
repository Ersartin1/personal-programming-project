﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;

public class StairsTile : CustomTile
{
    public enum Direction{Up, Down}
    public Direction StairDirection;

    [MenuItem("Assets/Create/Custom Tiles/Stairs Tile")]
    public static void CreatStairsTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Stairs Tile", "New Stairs Tile", "asset", "Save Stairs Tile", "Assets/Tiles");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<StairsTile>(), path);
    }

}