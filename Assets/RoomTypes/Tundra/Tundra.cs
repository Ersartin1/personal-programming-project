﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Tundra : RoomType
{
    public FloorTile Snow;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Initialize()
    {
        if (Snow != null)
        {
            Snow.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Snow);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile SnowFloor = Instantiate(Snow);
            SnowFloor.x = Floor.x;
            SnowFloor.y = Floor.y;
            SnowFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), SnowFloor);
            Room.Floors.Add(SnowFloor);
            Destroy(Floor);
        }
        temp.Clear();
        base.PopulateRoom(Room, Layout, FloorLayout);
    }
}

