﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Mineshaft : RoomType
{
    public FloorTile Stone;
    public FloorTile MinecartTrack;
    public CustomTile Minecart;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (Stone != null)
        {
            Stone.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Stone);
        }
        if (MinecartTrack != null)
        {
            MinecartTrack.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(MinecartTrack);
        }
        if (Minecart != null)
        {
            Minecart.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Minecart);
        }
        base.Initialize();
    }

    enum Direction { Right, Left, Up, Down };

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile newFloor = Instantiate(Stone);
            newFloor.x = Floor.x;
            newFloor.y = Floor.y;
            newFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newFloor);
            Room.Floors.Add(newFloor);
            Destroy(Floor);
        }
        temp.Clear();

        List<FloorTile> MineCartTracks = new List<FloorTile>();
        FloorTile CurrentTrack = Room.Floors[Random.Range(0, Room.Floors.Count - 1)];
        int Tracklength = Random.Range(5, 10);
        for(int i = 0; i < Tracklength; i++)
        {
            MineCartTracks.Add(CurrentTrack);
            FloorTile Floor = CurrentTrack;
            FloorTile newFloor = Instantiate(MinecartTrack);
            newFloor.x = Floor.x;
            newFloor.y = Floor.y;
            newFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newFloor);
            Room.Floors.Add(newFloor);
            Destroy(Floor);

            List<Direction> Directions = new List<Direction>(new Direction[] { Direction.Right, Direction.Left, Direction.Up, Direction.Down});

            bool DeadEnd = false;

            TileBase AdjacentTile = null;

            do
            {   
                if(Directions.Count == 0)
                {
                    DeadEnd = true;
                    break;
                }
                int randomIndex = Random.Range(0, Directions.Count);
                Direction NextDirection = Directions[randomIndex];
                Directions.RemoveAt(randomIndex);
                switch (NextDirection)
                {
                    case Direction.Right:
                        AdjacentTile = FloorLayout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1));
                        break;
                    case Direction.Left:
                        AdjacentTile = FloorLayout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1));
                        break;
                    case Direction.Down:
                        AdjacentTile = FloorLayout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1));
                        break;
                    case Direction.Up:
                        AdjacentTile = FloorLayout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1));
                        break;
                    default:
                        break;
                }
            } while (AdjacentTile as WallTile || AdjacentTile as DoorTile || (AdjacentTile as CustomTile).tileId == MinecartTrack.tileId);
            if (DeadEnd)
            {
                break;
            }
            CurrentTrack = AdjacentTile as FloorTile;
        }

        {
            int numSetpeices = (int)(Random.Range(1, 3) * Room.Floors.Count * (spawnRatio / 10));
            for (int i = 0; i < numSetpeices; i++)
            {
                CustomTile newSetpiece = Instantiate(Minecart);
                if (MineCartTracks.Count <= 0)
                {
                    break;
                }
                int index = Random.Range(0, MineCartTracks.Count - 1);
                FloorTile Floor = MineCartTracks[index];
                MineCartTracks.RemoveAt(index);
                Floor.isStairable = false;
                newSetpiece.x = Floor.x;
                newSetpiece.x = Floor.y;
                Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);
            }
        }

        foreach (FloorTile Floor in Room.Floors)
        {
            if (!Floor.Door && Floor.tileId != MinecartTrack.tileId)
                AvailableTiles.Add(Floor);
        }

        int count = 0;
        foreach (CustomTile SetPiece in SetPieces)
        {
            if (AvailableTiles.Count <= 0)
            {
                break;
            }

            int numSetpeices = (int)(Random.Range(SetPiecesSpawnAmount[count].x, SetPiecesSpawnAmount[count].y) * Room.Floors.Count * (spawnRatio / 10));

            for (int i = 0; i < numSetpeices; i++)
            {
                CustomTile newSetpiece = Instantiate(SetPiece);
                if (newSetpiece.name == "Crystal(Clone)")
                {
                    newSetpiece.color = new Color(Random.value, Random.value, Random.value);
                }
                if (AvailableTiles.Count <= 0)
                {
                    return;
                }
                int index = Random.Range(0, AvailableTiles.Count - 1);
                FloorTile Floor = AvailableTiles[index];
                AvailableTiles.RemoveAt(index);
                Floor.isStairable = false;
                newSetpiece.x = Floor.x;
                newSetpiece.x = Floor.y;
                Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);
            }
            count++;
        }
    }
}
