﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Swamp : RoomType
{
    public FloorTile SwampGrass;
    public FloorTile SwampDirt;
    public FloorTile SwampWater;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (SwampGrass != null)
        {
            SwampGrass.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(SwampGrass);
        }
        if (SwampDirt != null)
        {
            SwampDirt.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(SwampDirt);
        }
        if (SwampGrass != null)
        {
            SwampWater.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(SwampWater);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile newFloor;
            int choice = Random.Range(0, 3);
            if (choice == 0) {
                newFloor = Instantiate(SwampGrass);
            } else if(choice == 1)
            {
                newFloor = Instantiate(SwampDirt);
            } else
            {
                newFloor = Instantiate(SwampWater);
            }
            newFloor.x = Floor.x;
            newFloor.y = Floor.y;
            newFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newFloor);
            Room.Floors.Add(newFloor);
            Destroy(Floor);
        }
        temp.Clear();
        base.PopulateRoom(Room, Layout, FloorLayout);
    }
}
