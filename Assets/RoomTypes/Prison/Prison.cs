﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Prison : RoomType
{

    public CustomTile JailBars;
    public CustomTile JailDoor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (JailBars != null)
        {
            JailBars.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(JailBars);
        }
        if (JailDoor != null)
        {
            JailDoor.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(JailDoor);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<CustomTile>[] JailBarsLists = new List<CustomTile>[4];
        for (int i = 0; i < JailBarsLists.Length; i++)
        {
            JailBarsLists[i] = new List<CustomTile>();
        }
        Vector2Int Center = new Vector2Int(Room.Position.x + Room.RoomWidth / 2, Room.Position.y + Room.RoomHeight / 2);
        for(int i = Room.Position.x; i < Room.Position.x + Room.RoomWidth; i++)
        {
            FloorTile Floor = FloorLayout.GetTile(new Vector3Int(i, Center.y, 1)) as FloorTile;
            if (Floor && Floor.Room == Room)
            {
                CustomTile newJailBars = Instantiate(JailBars);
                newJailBars.x = i;
                newJailBars.y = Center.y;
                if (i < Center.x)
                {
                    JailBarsLists[0].Add(newJailBars);
                } else if (i > Center.x)
                {
                    JailBarsLists[1].Add(newJailBars);
                }
                Layout.SetTile(new Vector3Int(i, Center.y, 1), newJailBars);
            }
        }
        for (int i = Room.Position.y; i < Room.Position.y + Room.RoomHeight; i++)
        {
            FloorTile Floor = FloorLayout.GetTile(new Vector3Int(Center.x, i, 1)) as FloorTile;
            if (i != Center.y && Floor && Floor.Room == Room) 
            {
                CustomTile newJailBars = Instantiate(JailBars);
                newJailBars.x = Center.x;
                newJailBars.y = i;
                if (i < Center.y)
                {
                    JailBarsLists[2].Add(newJailBars);
                }
                else if (i > Center.y)
                {
                    JailBarsLists[3].Add(newJailBars);
                }
                Layout.SetTile(new Vector3Int(Center.x, i, 1), newJailBars);
            }
        }
        for (int i = 0; i < JailBarsLists.Length; i++)
        {
            if (JailBarsLists[i].Count > 0)
            {
                CustomTile JailbarToReplace = JailBarsLists[i][Random.Range(0, JailBarsLists[i].Count - 1)];
                CustomTile newJailDoor = Instantiate(JailDoor);
                newJailDoor.x = JailbarToReplace.x;
                newJailDoor.y = JailbarToReplace.y;
                Layout.SetTile(new Vector3Int(JailbarToReplace.x, JailbarToReplace.y, 1), newJailDoor);
                Destroy(JailbarToReplace);
            }
        }
        foreach (FloorTile Floor in Room.Floors)
        {
            if (!Floor.Door && Layout.GetTile(new Vector3Int(Floor.x, Floor.y, 1)) == null)
                AvailableTiles.Add(Floor);
        }
        int count = 0;
        foreach (CustomTile SetPiece in SetPieces)
        {
            if (AvailableTiles.Count <= 0)
            {
                break;
            }

            int numSetpeices = (int)(Random.Range(SetPiecesSpawnAmount[count].x, SetPiecesSpawnAmount[count].y) * Room.Floors.Count * (spawnRatio / 10));

            for (int i = 0; i < numSetpeices; i++)
            {
                CustomTile newSetpiece = Instantiate(SetPiece);
                if (AvailableTiles.Count <= 0)
                {
                    return;
                }
                int index = Random.Range(0, AvailableTiles.Count - 1);
                FloorTile Floor = AvailableTiles[index];
                AvailableTiles.RemoveAt(index);
                Floor.isStairable = false;
                newSetpiece.x = Floor.x;
                newSetpiece.x = Floor.y;
                Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);
            }
            count++;
        }
    }
}
