﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomType : MonoBehaviour
{
    public CustomTile[] SetPieces;
    public Vector2[] SetPiecesSpawnAmount;
    protected List<FloorTile> AvailableTiles = new List<FloorTile>();
    public float spawnRatio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Initialize()
    {
        foreach (CustomTile SetPiece in SetPieces)
        {
            if (SetPiece != null)
            {
                SetPiece.tileId = CustomTile.numberOfTileTypes++;
                DungeonLayoutGenerator.Typetable.Add(SetPiece);
            }
        }
    }

    public virtual void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        foreach (FloorTile Floor in Room.Floors)
        {
            if(!Floor.Door)
                AvailableTiles.Add(Floor);
        }
        int count = 0;
        foreach (CustomTile SetPiece in SetPieces)
        {
            if (AvailableTiles.Count <= 0)
            {
                break;
            }

            int numSetpeices = (int)(Random.Range(SetPiecesSpawnAmount[count].x, SetPiecesSpawnAmount[count].y) * Room.Floors.Count * (spawnRatio / 10));

            for (int i = 0; i < numSetpeices; i++)
            {
                CustomTile newSetpiece = Instantiate(SetPiece);
                if (AvailableTiles.Count <= 0)
                {
                    return;
                }
                int index = Random.Range(0, AvailableTiles.Count - 1);
                FloorTile Floor = AvailableTiles[index];
                AvailableTiles.RemoveAt(index);
                Floor.isStairable = false;
                newSetpiece.x = Floor.x;
                newSetpiece.x = Floor.y;
                Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);
            }
            count++;
        }
    }
}
