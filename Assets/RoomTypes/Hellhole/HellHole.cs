﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HellHole : RoomType
{
    public FloorTile HellStone;
    public FloorTile Lava;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void Initialize()
    {
        if (HellStone != null)
        {
            HellStone.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(HellStone);
        }
        if (Lava != null)
        {
            Lava.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Lava);
        }
        base.Initialize();
    }
    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> HellStoneFloors = new List<FloorTile>();
        List<FloorTile> LavaFloors = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            if (isNextToWall(Floor, FloorLayout))
                HellStoneFloors.Add(Floor);
            else
                LavaFloors.Add(Floor);
        }
        foreach (FloorTile Floor in HellStoneFloors)
        {
            FloorTile HellStoneFloor = Instantiate(HellStone);
            HellStoneFloor.x = Floor.x;
            HellStoneFloor.y = Floor.y;
            HellStoneFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), HellStoneFloor);
            Room.Floors.Add(HellStoneFloor);
            Destroy(Floor);
        }
        foreach(FloorTile Floor in LavaFloors)
        {
            FloorTile LavaFloor = Instantiate(Lava);
            LavaFloor.x = Floor.x;
            LavaFloor.y = Floor.y;
            LavaFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), LavaFloor);
            Room.Floors.Add(LavaFloor);
            Destroy(Floor);
        }
        base.PopulateRoom(Room, Layout, FloorLayout);
    }

    private bool isNextToWall(FloorTile Floor, Tilemap Layout)
    {
        TileBase AdjacentTileRight = Layout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1));
        TileBase AdjacentTileLeft = Layout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1));
        TileBase AdjacentTileUp = Layout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1));
        TileBase AdjacentTileDown = Layout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1));

        if (AdjacentTileRight as WallTile || AdjacentTileRight as DoorTile)
        {
            return true;
        }

        if (AdjacentTileLeft as WallTile || AdjacentTileLeft as DoorTile)
        {
            return true;
        }

        if (AdjacentTileUp as WallTile || AdjacentTileUp as DoorTile)
        {
            return true;
        }

        if (AdjacentTileDown as WallTile || AdjacentTileDown as DoorTile)
        {
            return true;
        }
        return false;
    }
}