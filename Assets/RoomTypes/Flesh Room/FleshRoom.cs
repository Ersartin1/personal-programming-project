﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FleshRoom : RoomType
{
    public FloorTile Flesh;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (Flesh != null)
        {
            Flesh.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Flesh);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile FleshFloor = Instantiate(Flesh);
            FleshFloor.x = Floor.x;
            FleshFloor.y = Floor.y;
            FleshFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), FleshFloor);
            Room.Floors.Add(FleshFloor);
            Destroy(Floor);
        }
        temp.Clear();
        base.PopulateRoom(Room, Layout, FloorLayout);
    }
}
