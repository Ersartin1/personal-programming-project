﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Beehive : RoomType
{
    public FloorTile HoneyComb;
    public CustomTile QueenBee;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (QueenBee != null)
        {
            QueenBee.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(QueenBee);
        }
        if (HoneyComb != null)
        {
            HoneyComb.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(HoneyComb);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile HoneycombFloor = Instantiate(HoneyComb);
            HoneycombFloor.x = Floor.x;
            HoneycombFloor.y = Floor.y;
            HoneycombFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), HoneycombFloor);
            Room.Floors.Add(HoneycombFloor);
            Destroy(Floor);
        }
        temp.Clear();
        base.PopulateRoom(Room, Layout, FloorLayout);
        CustomTile newSetpiece = Instantiate(QueenBee);
        if (AvailableTiles.Count <= 0)
        {
            return;
        }
        int index = Random.Range(0, AvailableTiles.Count - 1);
        FloorTile FloorTile = AvailableTiles[index];
        AvailableTiles.RemoveAt(index);
        FloorTile.isStairable = false;
        newSetpiece.x = FloorTile.x;
        newSetpiece.x = FloorTile.y;
        Layout.SetTile(new Vector3Int(FloorTile.x, FloorTile.y, 1), newSetpiece);
    }
}
