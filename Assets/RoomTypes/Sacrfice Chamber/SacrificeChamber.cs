﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SacrificeChamber : RoomType
{

    public FloorTile[] PentragramPieces = new FloorTile[9];
    public CustomTile Candles;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        Vector2Int Center = new Vector2Int(Room.Position.x + Room.RoomWidth / 2, Room.Position.y + Room.RoomHeight / 2);
        
        foreach (FloorTile Floor in Room.Floors)
        {
            if (!Floor.Door)
                AvailableTiles.Add(Floor);
        }

        int x = -1;
        int y = 1;

        foreach (FloorTile PentragramPeice in PentragramPieces)
        {
            FloorTile Floor = FloorLayout.GetTile(new Vector3Int(Center.x + x, Center.y + y, 1)) as FloorTile;
            if (Floor && Floor.Room == Room)
            {
                AvailableTiles.Remove(Floor);
                FloorTile newFloor = Instantiate(PentragramPeice);
                newFloor.x = Center.x + x;
                newFloor.y = Center.y + y;
                Layout.SetTile(new Vector3Int(newFloor.x, newFloor.y, 1), newFloor);
            }
            x++;
            if (x > 1)
            {
                x = -1;
                y--;
            }
        }

        PlaceCandle(Room, Layout, FloorLayout, Center.x, Center.y + 2);
        PlaceCandle(Room, Layout, FloorLayout, Center.x - 2, Center.y);
        PlaceCandle(Room, Layout, FloorLayout, Center.x + 2, Center.y);
        PlaceCandle(Room, Layout, FloorLayout, Center.x - 1, Center.y - 2);
        PlaceCandle(Room, Layout, FloorLayout, Center.x + 1, Center.y - 2);

        int count = 0;
        foreach (CustomTile SetPiece in SetPieces)
        {
            if (AvailableTiles.Count <= 0)
            {
                break;
            }

            int numSetpeices = (int)(Random.Range(SetPiecesSpawnAmount[count].x, SetPiecesSpawnAmount[count].y) * Room.Floors.Count * (spawnRatio / 10));

            for (int i = 0; i < numSetpeices; i++)
            {
                CustomTile newSetpiece = Instantiate(SetPiece);
                if (AvailableTiles.Count <= 0)
                {
                    return;
                }
                int index = Random.Range(0, AvailableTiles.Count - 1);
                FloorTile Floor = AvailableTiles[index];
                AvailableTiles.RemoveAt(index);
                Floor.isStairable = false;
                newSetpiece.x = Floor.x;
                newSetpiece.x = Floor.y;
                Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);
            }
            count++;
        }
    }

    private void PlaceCandle(Room room, Tilemap layout, Tilemap floorLayout, int v1, int v2)
    {
        FloorTile Floor = floorLayout.GetTile(new Vector3Int(v1, v2, 1)) as FloorTile;
        if (Floor && Floor.Room == room)
        {
            AvailableTiles.Remove(Floor);
            CustomTile newFloor = Instantiate(Candles);
            newFloor.x = v1;
            newFloor.y = v2;
            layout.SetTile(new Vector3Int(newFloor.x, newFloor.y, 1), newFloor);
        }
    }
}
