﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Lake : RoomType
{
    public FloorTile DeepWater;
    public FloorTile ShallowWater;
    public FloorTile Sand; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (Sand != null)
        {
            Sand.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Sand);
        }
        if (ShallowWater != null)
        {
            ShallowWater.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(ShallowWater);
        }
        if (DeepWater != null)
        {
            ShallowWater.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(DeepWater);
        }
        base.Initialize();
    }
    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> DeepWaterTiles = new List<FloorTile>();

        foreach (FloorTile Floor in Room.Floors)
        {
            DeepWaterTiles.Add(Floor);
        }

        foreach (FloorTile Floor in Room.FloorsNextToDoor)
        {
            FloorTile SandFloor = Instantiate(Sand);
            SandFloor.x = Floor.x;
            SandFloor.y = Floor.y;
            SandFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), SandFloor);
            Room.Floors.Add(SandFloor);
            DeepWaterTiles.Remove(Floor);
            Destroy(Floor);
            ConvertTileToSand(SandFloor.x + 1, SandFloor.y, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToSand(SandFloor.x - 1, SandFloor.y, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToSand(SandFloor.x, SandFloor.y + 1, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToSand(SandFloor.x, SandFloor.y - 1, Room, FloorLayout, DeepWaterTiles);

            ConvertTileToShallow(SandFloor.x + 2, SandFloor.y, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x - 2, SandFloor.y, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x, SandFloor.y + 2, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x, SandFloor.y - 2, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x + 1, SandFloor.y + 1, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x - 1, SandFloor.y + 1, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x + 1, SandFloor.y - 1, Room, FloorLayout, DeepWaterTiles);
            ConvertTileToShallow(SandFloor.x - 1, SandFloor.y - 1, Room, FloorLayout, DeepWaterTiles);
        }

        foreach (FloorTile Floor in DeepWaterTiles)
        {
            FloorTile DeepFloor = Instantiate(DeepWater);
            DeepFloor.x = Floor.x;
            DeepFloor.y = Floor.y;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), DeepFloor);
            Room.Floors.Add(DeepFloor);
            Destroy(Floor);
        }

        base.PopulateRoom(Room, Layout, FloorLayout);
    }

    private void ConvertTileToSand(int x, int y, Room Room, Tilemap Layout, List<FloorTile> DeepWaterTiles)
    {
        FloorTile Floor = Layout.GetTile(new Vector3Int(x, y, 1)) as FloorTile;
        if (Floor && Room.Floors.Contains(Floor))
        {
            FloorTile SandFloor = Instantiate(Sand);
            SandFloor.x = Floor.x;
            SandFloor.y = Floor.y;
            Room.Floors.Remove(Floor);
            Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), SandFloor);
            SandFloor.Door = Floor.Door;
            Room.Floors.Add(SandFloor);
            DeepWaterTiles.Remove(Floor);
            Destroy(Floor);
        }
    }
    private void ConvertTileToShallow(int x, int y, Room Room, Tilemap Layout, List<FloorTile> DeepWaterTiles)
    {
        FloorTile Floor = Layout.GetTile(new Vector3Int(x, y, 1)) as FloorTile;
        if (Floor && Floor.tileId != Sand.tileId && Room.Floors.Contains(Floor))
        {
            FloorTile ShallowFloor = Instantiate(ShallowWater);
            ShallowFloor.x = Floor.x;
            ShallowFloor.y = Floor.y;
            Room.Floors.Remove(Floor);
            Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), ShallowFloor);
            Room.Floors.Add(ShallowFloor);
            DeepWaterTiles.Remove(Floor);
            Destroy(Floor);
        }
    }
}
