﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Temple : RoomType
{
    public WallTile Window;
    public FloorTile Carpet;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void Initialize()
    {
        if (Carpet != null)
        {
            Carpet.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Carpet);
        }
        if (Window != null)
        {
            Window.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Window);
        }
        base.Initialize();
    }
    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> CarpetFloors = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            if (!isNextToWall(Floor, FloorLayout))
                CarpetFloors.Add(Floor);
        }
        foreach (FloorTile Floor in CarpetFloors)
        {
            FloorTile CarpetFloor = Instantiate(Carpet);
            CarpetFloor.x = Floor.x;
            CarpetFloor.y = Floor.y;
            CarpetFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), CarpetFloor);
            Room.Floors.Add(CarpetFloor);
            Destroy(Floor);
        }
        int numWindows = (int)(Random.Range(5, 10) * Room.Floors.Count * (spawnRatio / 10));
        for(int i = 0; i < numWindows; i++)
        {
            WallTile WallToReplace = Room.Walls[Random.Range(0, Room.Walls.Count - 1)];
            WallTile WindowTile = Instantiate(Window);
            WindowTile.x = WallToReplace.x;
            WindowTile.y = WallToReplace.y;
            Room.Walls.Remove(WallToReplace);
            FloorLayout.SetTile(new Vector3Int(WallToReplace.x, WallToReplace.y, 1), WindowTile);
            Destroy(WallToReplace);
        }
        base.PopulateRoom(Room, Layout, FloorLayout);
    }

    private bool isNextToWall(FloorTile Floor, Tilemap Layout)
    {
        TileBase AdjacentTileRight = Layout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1));
        TileBase AdjacentTileLeft = Layout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1));
        TileBase AdjacentTileUp = Layout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1));
        TileBase AdjacentTileDown = Layout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1));

        if (AdjacentTileRight as WallTile || AdjacentTileRight as DoorTile)
        {
            return true;
        }

        if (AdjacentTileLeft as WallTile || AdjacentTileLeft as DoorTile)
        {
            return true;
        }

        if (AdjacentTileUp as WallTile || AdjacentTileUp as DoorTile)
        {
            return true;
        }

        if (AdjacentTileDown as WallTile || AdjacentTileDown as DoorTile)
        {
            return true;
        }
        return false;
    }
}
