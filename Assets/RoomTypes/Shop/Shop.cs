﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Shop : RoomType
{
    public CustomTile ShopKeeper;
    public CustomTile Table;
    private List<CustomTile> TableSpaces = new List<CustomTile>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Initialize()
    {
        if (ShopKeeper != null)
        {
            ShopKeeper.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(ShopKeeper);
        }
        if (Table != null)
        {
            Table.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Table);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        foreach (FloorTile FloorTile in Room.Floors)
        {
            if (!FloorTile.Door)
                AvailableTiles.Add(FloorTile);
        }

        CustomTile newSetpiece = Instantiate(ShopKeeper);
        if (AvailableTiles.Count <= 0)
        {
            return;
        }
        int index = Random.Range(0, AvailableTiles.Count - 1);
        FloorTile Floor = AvailableTiles[index];
        AvailableTiles.RemoveAt(index);
        Floor.isStairable = false;
        newSetpiece.x = Floor.x;
        newSetpiece.x = Floor.y;
        Layout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);

        int numSetpeices = (int)(Random.Range(10, 15) * Room.Floors.Count * (spawnRatio / 10));

        for (int i = 0; i < numSetpeices; i++)
        {
            newSetpiece = Instantiate(Table);
            if (AvailableTiles.Count <= 0)
            {
                return;
            }
            index = Random.Range(0, AvailableTiles.Count - 1);
            Floor = AvailableTiles[index];
            AvailableTiles.RemoveAt(index);
            Floor.isStairable = false;
            newSetpiece.x = Floor.x;
            newSetpiece.y = Floor.y;
            Destroy(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), newSetpiece);
            TableSpaces.Add(newSetpiece);
        }

        foreach(CustomTile Table in TableSpaces)
        {
            newSetpiece = Instantiate(SetPieces[Random.Range(0, SetPieces.Length-1)]);
            newSetpiece.x = Table.x;
            newSetpiece.x = Table.y;
            Layout.SetTile(new Vector3Int(Table.x, Table.y, 1), newSetpiece);
        }
    }
}
