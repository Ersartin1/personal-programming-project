﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Forest : RoomType
{
    public FloorTile Grass;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Initialize()
    {
        if (Grass != null)
        {
            Grass.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Grass);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile GrassFloor = Instantiate(Grass);
            GrassFloor.x = Floor.x;
            GrassFloor.y = Floor.y;
            GrassFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), GrassFloor);
            Room.Floors.Add(GrassFloor);
            Destroy(Floor);
        }
        temp.Clear();
        base.PopulateRoom(Room, Layout, FloorLayout);
    }
}
