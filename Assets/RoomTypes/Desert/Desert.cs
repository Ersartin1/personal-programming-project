﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Desert : RoomType
{
    public FloorTile Sand;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Initialize()
    {
        if (Sand != null)
        {
            Sand.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Sand);
        }
        base.Initialize();
    }

    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> temp = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            temp.Add(Floor);
        }
        foreach (FloorTile Floor in temp)
        {
            FloorTile SandFloor = Instantiate(Sand);
            SandFloor.x = Floor.x;
            SandFloor.y = Floor.y;
            SandFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), SandFloor);
            Room.Floors.Add(SandFloor);
            Destroy(Floor);
        }
        temp.Clear();
        base.PopulateRoom(Room, Layout, FloorLayout);
    }
}
