﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Sewer : RoomType
{
    public FloorTile Concrete;
    public FloorTile Sewage;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void Initialize()
    {
        if (Concrete != null)
        {
            Concrete.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Concrete);
        }
        if (Sewage != null)
        {
            Sewage.tileId = CustomTile.numberOfTileTypes++;
            DungeonLayoutGenerator.Typetable.Add(Sewage);
        }
        base.Initialize();
    }
    public override void PopulateRoom(Room Room, Tilemap Layout, Tilemap FloorLayout)
    {
        List<FloorTile> ConcreteFloors = new List<FloorTile>();
        List<FloorTile> SewageFloors = new List<FloorTile>();
        foreach (FloorTile Floor in Room.Floors)
        {
            if (isNextToWall(Floor, FloorLayout))
                ConcreteFloors.Add(Floor);
            else
                SewageFloors.Add(Floor);
        }
        foreach (FloorTile Floor in ConcreteFloors)
        {
            FloorTile ConcreteFloor = Instantiate(Concrete);
            ConcreteFloor.x = Floor.x;
            ConcreteFloor.y = Floor.y;
            ConcreteFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), ConcreteFloor);
            Room.Floors.Add(ConcreteFloor);
            Destroy(Floor);
        }
        foreach (FloorTile Floor in SewageFloors)
        {
            FloorTile SewageFloor = Instantiate(Sewage);
            SewageFloor.x = Floor.x;
            SewageFloor.y = Floor.y;
            SewageFloor.Door = Floor.Door;
            Room.Floors.Remove(Floor);
            FloorLayout.SetTile(new Vector3Int(Floor.x, Floor.y, 1), SewageFloor);
            Room.Floors.Add(SewageFloor);
            Destroy(Floor);
        }
        base.PopulateRoom(Room, Layout, FloorLayout);
    }

    private bool isNextToWall(FloorTile Floor, Tilemap Layout)
    {
        TileBase AdjacentTileRight = Layout.GetTile(new Vector3Int(Floor.x + 1, Floor.y, 1));
        TileBase AdjacentTileLeft = Layout.GetTile(new Vector3Int(Floor.x - 1, Floor.y, 1));
        TileBase AdjacentTileUp = Layout.GetTile(new Vector3Int(Floor.x, Floor.y + 1, 1));
        TileBase AdjacentTileDown = Layout.GetTile(new Vector3Int(Floor.x, Floor.y - 1, 1));

        if (AdjacentTileRight as WallTile || AdjacentTileRight as DoorTile)
        {
            return true;
        }

        if (AdjacentTileLeft as WallTile || AdjacentTileLeft as DoorTile)
        {
            return true;
        }

        if (AdjacentTileUp as WallTile || AdjacentTileUp as DoorTile)
        {
            return true;
        }

        if (AdjacentTileDown as WallTile || AdjacentTileDown as DoorTile)
        {
            return true;
        }
        return false;
    }
}
